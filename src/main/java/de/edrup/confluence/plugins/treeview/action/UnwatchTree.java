package de.edrup.confluence.plugins.treeview.action;



import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;

import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

public class UnwatchTree extends ConfluenceActionSupport implements PageAware {

	static final long serialVersionUID = 42L;
	
	private AbstractPage page;
	private TreeViewHelper treeViewHelper;
		
	public UnwatchTree(/*TreeViewHelper treeViewHelper*/) {
        //this.treeViewHelper = treeViewHelper;
	}
	
    // actions don't eat the constructor injection
    public void setTreeViewHelper(TreeViewHelper treeViewHelper) {
        this.treeViewHelper = treeViewHelper;
    }

	@Override
	public AbstractPage getPage() {
		return page;
	}

	@Override
	public boolean isLatestVersionRequired() {
		return true;
	}

	@Override
	public boolean isPageRequired() {
		return true;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	@Override
	public void setPage(AbstractPage page) {
		this.page = page;
	}
	
	// action which is initially called for un-watch
	public String execute() throws Exception
	{
		// the page we want to un-watch
		Page p = (Page) getPage();
		
		// read the big_tree_limit from the properties
		int big_tree_limit = Integer.parseInt(this.getText("de.edrup.confluence.plugins.tree-view.bigTree.limit", "20"));
		
		// in case the number of descendants is above the limit
		if(p.getDescendants().size() > big_tree_limit) {
			return "bigtree";
		}
		// if not un-watch without asking again
		else {
			return doUnwatchTree();
		}
	}

	// do the un-watch work
	public String doUnwatchTree() throws Exception {
		// get the user currently logged in
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// is it a named user?
		if(confluenceUser != null) {
			
			// remove the watch from our list
			treeViewHelper.removeWatch(confluenceUser, (Page) getPage());
			
			// set the watch flag for this page an all descendants
			treeViewHelper.setWatchFlagInTransactionAsync(confluenceUser, (Page) getPage(), false);			
		}
		return "success";
	}
	
	// return the size of the tree the user wants to unwatch
	public String getTreeSize() {
		Page p = (Page) getPage();
		Integer s = p.getDescendants().size() + 1;
		return s.toString();
	}
}
