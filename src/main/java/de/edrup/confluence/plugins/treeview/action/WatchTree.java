package de.edrup.confluence.plugins.treeview.action;


import javax.inject.Inject;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;

import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

public class WatchTree extends ConfluenceActionSupport implements PageAware {

	static final long serialVersionUID = 42L;
	
	private AbstractPage page;
	final TreeViewHelper treeViewHelper;
	
	
	@Inject
	public WatchTree(TreeViewHelper treeViewHelper) {
        this.treeViewHelper = treeViewHelper;
	}

	@Override
	public AbstractPage getPage() {
		return page;
	}

	@Override
	public boolean isLatestVersionRequired() {
		return true;
	}

	@Override
	public boolean isPageRequired() {
		return true;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	@Override
	public void setPage(AbstractPage page) {
		this.page = page;
	}
	
	public String execute()
	{
		// get the user currently logged in
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// is it a named user?
		if(confluenceUser != null) {
			
			// add the tree view to our list
			treeViewHelper.addWatch(confluenceUser, (Page) getPage());
			
			// set the watch flag for this page an all descendants
			treeViewHelper.setWatchFlagInTransactionAsync(confluenceUser, (Page) getPage(), true);					
		}
		return "success";
	}
}
