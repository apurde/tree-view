package de.edrup.confluence.plugins.treeview.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.confluence.api.service.accessmode.ReadOnlyAccessBlocked;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;

import de.edrup.confluence.plugins.treeview.ao.TreeWatchService;
import de.edrup.confluence.plugins.treeview.ao.TreeWatch;
import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

@Path("/")
public class TreeViewRest {
	
	private final UserAccessor userAcc;
	private final TreeViewHelper treeViewHelper;
	private final PageManager pageMan;
	private final PermissionManager permissionMan;
	private final TreeWatchService treeWatchService;

	
	@Inject
	public TreeViewRest(@ComponentImport UserAccessor userAcc, TreeViewHelper treeViewHelper, @ComponentImport PageManager pageMan,
		@ComponentImport PermissionManager permissionMan, TreeWatchService treeWatchService) {
		this.userAcc = userAcc;
		this.treeViewHelper = treeViewHelper;
		this.pageMan = pageMan;
		this.permissionMan = permissionMan;
		this.treeWatchService = treeWatchService;
	}

	
	// check if the page ID given is covered by a tree watch of the user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/isCovered")
    public Response isCovered(@QueryParam("username") String username, @PathParam("id") Long pageId)
    {
		// do our pre-handling
		ConfluenceUser user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		Response r = handleParamsAndCheckRights(username, pageId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// check if page is affected by any watch of the given user
		Long treeWatchID = treeViewHelper.isUserTreeWatchingPage(pageId, user.getKey().getStringValue());
		if(treeWatchID > 0L) {
			return Response.ok(new StringResponse("true;" + treeWatchID.toString())).build();
		}
		else {
			return Response.ok(new StringResponse("false")).cacheControl(getNoStoreNoCacheControl()).build();
		}
    }
	
	
	// set a tree watch for the given page ID and user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/watchTree")
	@ReadOnlyAccessBlocked
    public Response watchTree(@QueryParam("username") String username, @PathParam("id") Long pageId)
    {
		// do our pre-handling
		ConfluenceUser user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		Response r = handleParamsAndCheckRights(username, pageId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// add the tree view to our list
		treeViewHelper.addWatch(user, pageMan.getPage(pageId));
		
		// set the watch flag for this page an all descendants
		treeViewHelper.setWatchFlagInTransactionAsync(user, pageMan.getPage(pageId), true);
							
		// return success
		return Response.ok(new StringResponse("success")).cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	// remove a tree watch for the given page ID and user
	// in case username is not given the calling user is used
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/unwatchTree")
	@ReadOnlyAccessBlocked
    public Response unwatchTree(@QueryParam("username") String username, @PathParam("id") Long pageId)
    {
		// do our pre-handling
		ConfluenceUser user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		Response r = handleParamsAndCheckRights(username, pageId);
		if(r.getStatus() != Status.OK.getStatusCode()) {
			return r;
		}
		
		// add the tree view to our list
		treeViewHelper.removeWatch(user, pageMan.getPage(pageId));
		
		// set the watch flag for this page an all descendants
		treeViewHelper.setWatchFlagInTransactionAsync(user, pageMan.getPage(pageId), false);
							
		// respond success
		return Response.ok(new StringResponse("success")).cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	// return all tree watches (of a user)
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/getTreeWatches")
	public Response getTreeWatches(@QueryParam("username") String username) {
		return getTreeWatchesInternal(username);
	}
	
	
	// migration 
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/migration")
	public Response getTreeWatches() {
		return getTreeWatchesInternal(null);
	}
	
	
	private Response getTreeWatchesInternal(String username) {
		ConfluenceUser user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		if(!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get())) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		// get all tree watches
		List<TreeWatch> watches = (username != null) ? treeWatchService.getAllTreeWatchesOfUser(user.getKey().getStringValue()) : treeWatchService.getAllTreeWatches();
		
		// build the response
		ArrayList<WatchResponse> watchesREST = new ArrayList<WatchResponse>();
		for(TreeWatch watch:watches) {
			ConfluenceUser watchUser = userAcc.getUserByKey(new UserKey(watch.getUserKey()));
			watchesREST.add(new WatchResponse(watch.getPageId(), (watchUser != null) ? watchUser.getName() : "unknown", watchUser.getFullName(), pageMan.getAbstractPage(watch.getPageId()).getSpace().getKey(), pageMan.getAbstractPage(watch.getPageId()).getTitle()));
		}

		return Response.ok(new WatchesResponse(watchesREST)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	// return all tree watches of a page
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{id}/getTreeWatches")
	public Response getTreeWatchesOfPage(@PathParam("id") Long pageId) {
		
		// action only allowed for admins
		if(!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get())) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		// get all tree watches
		List<TreeWatch> watches = treeWatchService.getAllTreeWatchesForPage(pageId);
		
		// build the response
		ArrayList<WatchResponse> watchesREST = new ArrayList<WatchResponse>();
		for(TreeWatch watch:watches) {
			ConfluenceUser watchUser = userAcc.getUserByKey(new UserKey(watch.getUserKey()));
			watchesREST.add(new WatchResponse(watch.getPageId(), (watchUser != null) ? watchUser.getName() : "unknown", watchUser.getFullName(), pageMan.getAbstractPage(watch.getPageId()).getSpace().getKey(), pageMan.getAbstractPage(watch.getPageId()).getTitle()));
		}

		return Response.ok(new WatchesResponse(watchesREST)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	// return all of my tree watches
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/getMyTreeWatches")
	public Response getMyTreeWatches() {
		
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
			
		List<TreeWatch> watches = treeWatchService.getAllTreeWatchesOfUser(user.getKey().getStringValue());
	
		// build response
		ArrayList<WatchResponse> watchesREST = new ArrayList<WatchResponse>();
		for(TreeWatch watch:watches) {
			AbstractPage p = pageMan.getAbstractPage(watch.getPageId());
			if(permissionMan.hasPermission(user, Permission.VIEW, p)) {
				ConfluenceUser watchUser = userAcc.getUserByKey(new UserKey(watch.getUserKey()));
				watchesREST.add(new WatchResponse(watch.getPageId(), (watchUser != null) ? watchUser.getName() : "unknown", watchUser.getFullName(), p.getSpace().getKey(), p.getTitle()));;
			}
		}

		return Response.ok(new WatchesResponse(watchesREST)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	// is licensed?
	@GET
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/isLicensed")
    public Response isLicensed()
    {
		return Response.ok(treeViewHelper.isLicensed() ? "{\"licensed\":true}" : "{\"licensed\":false}").build();
    }
	
	
	// handle the parameters username and contentId => user, p and check the proper rights of this REST request
	private Response handleParamsAndCheckRights(String username, Long pageId) {
		// the user for which we going to check
		ConfluenceUser user = (username == null) ? AuthenticatedUserThreadLocal.get() : userAcc.getUserByName(username);
		
		// get the page corresponding to the given ID
		Page p = pageMan.getPage(pageId);
		
		// negative response in case the page or user does not exist
		if((p == null) || (user == null)) {
			return Response.status(Status.NOT_FOUND).build();
		}
		else {
			// check whether user is allowed to view the page and whether a non admin asks for a another user
			// remark: "another" is not checked here directly - once the username is specified we assume an admin call
			if(!permissionMan.hasPermission(user, Permission.VIEW, p) || ((username != null) &&
					!permissionMan.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get()))) {
				return Response.status(Status.FORBIDDEN).build();
			}
			// everything ok
			else {
				return Response.status(Status.OK).cacheControl(getNoStoreNoCacheControl()).build();
			}
		}
	}
	
	
	// no store no cache header
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
}