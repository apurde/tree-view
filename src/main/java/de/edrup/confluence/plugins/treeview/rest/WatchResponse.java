package de.edrup.confluence.plugins.treeview.rest;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "treeWatch")
@XmlAccessorType(XmlAccessType.FIELD)
public class WatchResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "id")
	private Long pageID;
	
	@XmlElement(name = "username")
	private String userName;
	
	@XmlElement(name = "fullname")
	private String fullname;
	
	@XmlElement(name = "spacekey")
	private String spacekey;
	
	@XmlElement(name = "title")
	private String title;
	
	public WatchResponse() {
	}
	
	public WatchResponse(Long pageID, String userName, String fullName, String spaceKey, String title) {
		this.pageID = pageID;
		this.userName = userName;
		this.fullname = fullName;
		this.spacekey = spaceKey;
		this.title = title;
	}
}
