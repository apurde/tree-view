package de.edrup.confluence.plugins.treeview.rest;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class StringResponse {
	
	@XmlElement(name = "result")
	private String response;
	
	public StringResponse() {
	}
	
	public StringResponse(String response) {
		this.response = response;
	}
}
