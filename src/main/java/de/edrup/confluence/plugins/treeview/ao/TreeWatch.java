package de.edrup.confluence.plugins.treeview.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;

@Preload
public interface TreeWatch extends Entity {

	@NotNull
	void setPageId(Long pageId);
	Long getPageId();
		
	@NotNull
	void setUserKey(String userKey);
	String getUserKey();	
}
