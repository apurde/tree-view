package de.edrup.confluence.plugins.treeview.listener;

import javax.inject.Inject;

import org.springframework.beans.factory.DisposableBean;

import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageMoveEvent;
import com.atlassian.event.api.EventListener;

import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

public class TreeViewListener implements DisposableBean{

	private final TreeViewHelper treeViewHelper;
	//private final EventPublisher eventPub;
	
	
	@Inject
	public TreeViewListener(TreeViewHelper treeViewHelper/*, EventPublisher eventPub*/) {
		this.treeViewHelper = treeViewHelper;
		//this.eventPub = eventPub;
		//eventPub.register(this);
	}
	
	
	@EventListener
    public void onPageCreateEvent(PageCreateEvent event) {
		treeViewHelper.handleAddAndMoveAsync(event.getPage().getId(), 0L);
    }

	
	@EventListener
    public void onPageMoveEvent(PageMoveEvent event) {
		// we only trigger our work for the parent page which is moved
		if(!event.isMovedBecauseOfParent()) {
			treeViewHelper.handleAddAndMoveAsync(event.getPage().getId(), event.getOldParentPage().getId());
		}
    }
	
	
	@Override
	public void destroy() throws Exception {
		//eventPub.unregister(this);
	}
}
