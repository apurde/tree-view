package de.edrup.confluence.plugins.treeview.macro;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserKey;

import de.edrup.confluence.plugins.treeview.ao.TreeWatch;
import de.edrup.confluence.plugins.treeview.ao.TreeWatchService;
import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

// this macro shows the list of all tree views (mainly for debug reasons)
public class ShowTreeViews implements Macro {
	
	private final TreeViewHelper treeViewHelper;
	private final UserAccessor userAcc;
	private final PermissionManager permissionMan;
	private final PageManager pageManager;
	private final I18nResolver i18n;
	private final TreeWatchService treeWatchService;

	@Inject
	public ShowTreeViews(TreeViewHelper treeViewHelper, @ComponentImport UserAccessor userAcc, @ComponentImport PageManager pageManager,
		@ComponentImport PermissionManager permissionMan, @ComponentImport I18nResolver i18n, TreeWatchService treeWatchService) {
		this.treeViewHelper = treeViewHelper;
		this.userAcc = userAcc;
		this.pageManager = pageManager;
		this.permissionMan = permissionMan;
		this.i18n = i18n;
		this.treeWatchService = treeWatchService;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String body,
			ConversionContext context) throws MacroExecutionException {
		
		// get the user currently logged in
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// in case we have to show all tree views the user has to be an admin
		if(!permissionMan.isConfluenceAdministrator(confluenceUser)) {
			return i18n.getText("de.edrup.confluence.plugins.tree-view.tree-views.message.admin");
		}

		// we put all our output in a StringBuilder
		StringBuilder sb = new StringBuilder();
				
		// split the list of watches into single watches
		List<TreeWatch> watches = treeWatchService.getAllTreeWatches();
		
		// for all single watches
		for(TreeWatch watch : watches) {
			
			// get user from the watch string
			ConfluenceUser u = userAcc.getExistingUserByKey(new UserKey(watch.getUserKey()));
			
			// if user is existing
			if(u != null) {
				// start of paragraph
				sb.append("<p>");
				
				// append user name
				sb.append(u.getFullName());
				sb.append(": ");
				
				// page name
				Page p = pageManager.getPage(watch.getPageId());
				if(p != null) {
					sb.append(treeViewHelper.getTitleAsLink(p));
				}
				else {
					sb.append(i18n.getText("de.edrup.confluence.plugins.tree-view.tree-views.message.page"));
				}
				
				// end of paragraph
				sb.append("</p>");
			}
		}
		
		return sb.toString();
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
