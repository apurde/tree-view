package de.edrup.confluence.plugins.treeview.ao.upgrade.v1;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.treeview.ao.TreeWatch;
import net.java.ao.DBParam;
import net.java.ao.Query;


public class TreeViewAOUpgradeTask implements ActiveObjectsUpgradeTask {
	
	private final BandanaManager bandanaMan;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewAOUpgradeTask.class);
	
	
	@Inject
	public TreeViewAOUpgradeTask(@ComponentImport BandanaManager bandanaMan) {
		this.bandanaMan = bandanaMan;
	}

	
	@Override
	public ModelVersion getModelVersion() {
		return ModelVersion.valueOf("1");
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void upgrade(ModelVersion modelVersion, ActiveObjects ao) {
		
		ao.migrate(TreeWatch.class);
		
		// get legacy watches 
		String legacyWatchesString = (String) bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.active-watches");
		if(legacyWatchesString == null) {
			legacyWatchesString = "";
		}
		String[] watches = legacyWatchesString.split(";");
		
		// loop through all legacy watches
		for(String watch : watches) {
			if(!watch.isEmpty()) {
				try {
					Long pageId = Long.parseLong(watch.substring(watch.indexOf(":") + 1));
					String userKey = watch.substring(0, watch.indexOf(":"));
					if(ao.count(TreeWatch.class, Query.select().where("PAGE_ID = ? AND USER_KEY = ?", pageId, userKey)) == 0) {
						final TreeWatch treeWatch = ao.create(TreeWatch.class, new DBParam("PAGE_ID", pageId), new DBParam("USER_KEY", userKey));
						treeWatch.save();
					}
				}
				catch(Exception e) {
					log.error("Error converting watch {}: {}", watch, e.toString());
				}
			}
		}
	}
}
