package de.edrup.confluence.plugins.treeview.ao;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import net.java.ao.DBParam;
import net.java.ao.Query;

@Named
public class TreeWatchService {
	
	private final ActiveObjects ao;
	
	
	@Inject
	public TreeWatchService(@ComponentImport ActiveObjects ao) {
		this.ao = ao;
	}

	
	public List<TreeWatch> getAllTreeWatches() {
		return Arrays.asList(ao.find(TreeWatch.class, Query.select()));
	}
	
	
	public List<TreeWatch> getAllTreeWatchesOfUser(String userKey) {
		return Arrays.asList(ao.find(TreeWatch.class, Query.select().where("USER_KEY = ?", userKey)));
	}
	
	
	public List<TreeWatch> getAllTreeWatchesForPage(Long pageId) {
		return Arrays.asList(ao.find(TreeWatch.class, Query.select().where("PAGE_ID = ?", pageId)));
	}

	
	public TreeWatch addWatch(Long pageId, String userKey) {
		 final TreeWatch treeWatch = ao.create(TreeWatch.class, new DBParam("PAGE_ID", pageId), new DBParam("USER_KEY", userKey));
		 treeWatch.save();
		 return treeWatch;
	}
	
	
	public boolean isWatching(Long pageId, String userKey) {
		return ao.count(TreeWatch.class, Query.select().where("PAGE_ID = ? AND USER_KEY = ?", pageId, userKey)) > 0 ? true : false;
	}
	
	
	public void removeWatch(Long pageId, String userKey) {
		ao.delete(ao.find(TreeWatch.class, Query.select().where("PAGE_ID = ? AND USER_KEY = ?", pageId, userKey)));
	}
	
	
	public void removeAllWatchesOfUser(String userKey) {
		ao.delete(ao.find(TreeWatch.class, Query.select().where("USER_KEY = ?", userKey)));
	}
}
