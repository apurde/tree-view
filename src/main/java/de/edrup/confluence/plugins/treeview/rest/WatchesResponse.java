package de.edrup.confluence.plugins.treeview.rest;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "treeWatches")
@XmlAccessorType(XmlAccessType.FIELD)
public class WatchesResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "treeWatch")
	private ArrayList<WatchResponse> watches;
	
	public WatchesResponse() {
	}
	
	public WatchesResponse(ArrayList<WatchResponse> watches) {
		this.watches = watches;
	}
}
