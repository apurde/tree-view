package de.edrup.confluence.plugins.treeview.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;

import de.edrup.confluence.plugins.treeview.core.TreeViewHelper;

@Named
public class TreeViewCleaner implements JobRunner {

	private final TreeViewHelper treeViewHelper;
	private final TransactionTemplate transactionTemplate;
	private final AccessModeService accessModeService;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewCleaner.class);

	
	@Inject
	public TreeViewCleaner(TreeViewHelper treeViewHelper,  @ComponentImport TransactionTemplate transactionTemplate,  @ComponentImport AccessModeService accessModeService) {
		this.treeViewHelper = treeViewHelper;
		this.transactionTemplate = transactionTemplate;
		this.accessModeService = accessModeService;
	}

	
	// things do do in the task
	@Override
	public JobRunnerResponse runJob(JobRunnerRequest request) {
		
		if(accessModeService.isReadOnlyAccessModeEnabled()) { return JobRunnerResponse.success(); }
		
		// as we can't be sure that the connection to teh DB is open we have to do this in a transaction
		transactionTemplate.execute(new TransactionCallback<Object>()
		{
		    @Override
		    public Object doInTransaction()
		    {
		    	log.info("Cleaner task was started!");
		    	treeViewHelper.checkAllWatches();
		    	return null;
		    }
		});
		
		return JobRunnerResponse.success();
	}
}
