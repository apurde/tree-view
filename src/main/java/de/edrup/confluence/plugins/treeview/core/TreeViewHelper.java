package de.edrup.confluence.plugins.treeview.core;

import java.util.Date;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.jmx.JmxSMTPMailServer;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.upm.api.license.PluginLicenseManager;

import de.edrup.confluence.plugins.TreeViewConfiguration;
import de.edrup.confluence.plugins.treeview.ao.TreeWatch;
import de.edrup.confluence.plugins.treeview.ao.TreeWatchService;

import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;


// the TreeViewHelper class offers all functionality for handling tree-viewing
@Named
public class TreeViewHelper {
	
	private final BandanaManager bandanaMan;
	private final PageManager pageMan;
	private final NotificationManager notificationMan;
	private final UserAccessor userAcc;
	private final MultiQueueTaskManager mqtm;
	private final SettingsManager settingsMan;
	private final PermissionManager permissionMan;
	private final I18nResolver i18n;
	private final TransactionTemplate transactionTemplate;
	private final XhtmlContent xhtmlUtils;
	private final MailServerManager mailServerMan;
	private final TreeWatchService treeWatchService;
	private final PluginLicenseManager pluginLicenseMan;
	
	private final Cache<String, String> bodyCache;
	
	private TreeViewConfiguration configuration;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewHelper.class);
	private static ExecutorService pool = Executors.newFixedThreadPool(4);    
	
	private final int BATCH_SIZE = 20;
	
	// constructor
	@Inject
	public TreeViewHelper(@ComponentImport BandanaManager bandanaMan, @ComponentImport PageManager pageMan,
		@ComponentImport NotificationManager notificationMan, @ComponentImport UserAccessor userAcc,
		@ComponentImport MultiQueueTaskManager mqtm, @ComponentImport SettingsManager settingsMan,
		@ComponentImport PermissionManager permissionMan, @ComponentImport I18nResolver i18n,
		@ComponentImport CacheManager cacheMan, @ComponentImport TransactionTemplate transactionTemplate,
		@ComponentImport XhtmlContent xhtmlUtils, TreeWatchService treeWatchService, 
		@ComponentImport PluginLicenseManager pluginLicenseMan) {
		this.bandanaMan = bandanaMan;
		this.pageMan = pageMan;
		this.notificationMan = notificationMan;
		this.userAcc = userAcc;
		this.mqtm = mqtm;
		this.settingsMan  = settingsMan;
		this.permissionMan = permissionMan;
		this.i18n = i18n;
		this.transactionTemplate = transactionTemplate;
		this.xhtmlUtils = xhtmlUtils;
		this.treeWatchService = treeWatchService;
		this.pluginLicenseMan = pluginLicenseMan;
		this.mailServerMan = MailFactory.getServerManager();
		
		bodyCache = cacheMan.getCache("Tree View email body cache", 
			null, new CacheSettingsBuilder().local().expireAfterAccess(30, TimeUnit.SECONDS).build());
	}
	
	
	// add a watch to the list
	public void addWatch(ConfluenceUser u, Page page) {
		
		if(!isLicensed()) { return; }
			
		if(!treeWatchService.isWatching(page.getId(), u.getKey().getStringValue())) {
			treeWatchService.addWatch(page.getId(), u.getKey().getStringValue());
			notificationMan.addContentNotification(u, page);
			log.info("A tree-watch was added for {} starting page {}.", u.getFullName(), page.getTitle());
		}
	}
	
	
	// remove a watch from the list
	public void removeWatch(ConfluenceUser u, Page page) {
		
		if(!isLicensed()) { return; }
		
		treeWatchService.removeWatch(page.getId(), u.getKey().getStringValue());
		notificationMan.removeContentNotification(u, page);
		log.info("The tree watch for {} starting page {} has been removed.", u.getFullName(), page.getTitle());		
	}
	
	
	// set or un-set the watch flag in a transaction
	public void setWatchFlagInTransactionAsync(ConfluenceUser user, Page startPage, boolean set) {
		CompletableFuture.supplyAsync(() -> setWatchFlag(user.getKey().getStringValue(), startPage.getId(), set), pool);
	}
	
	
	// set or un-set the watch flag for the startPage and all its descendants
	private boolean setWatchFlag(String userKey, Long startPageId, boolean set) {
		
		if(!isLicensed()) { return false; }
		
		Date start = new Date();
				
		// get all descendants including the start page
		ArrayList<Long> descIds = new ArrayList<Long>();
		descIds.add(startPageId);
		descIds.addAll(pageMan.getDescendantIds(pageMan.getPage(startPageId)));
		log.debug("Setting/removing the watch flag on {} descendants", descIds.size());

		// loop through all descendants and set/un-set the watch
		int fromIndex = 0;
		while(fromIndex < descIds.size()) {
			try {
				int toIndex = ((fromIndex + BATCH_SIZE) < descIds.size()) ? fromIndex + BATCH_SIZE : descIds.size();  
				setWatchFlagInBatchInTransaction(userKey, descIds.subList(fromIndex, toIndex), set);
			}
			catch(Exception e) {
				log.error(e.toString());
			}
			fromIndex += BATCH_SIZE;
		}
		
		Date end = new Date();
		log.debug("Setting/unsetting all watch flags on page {} took {} ms.", startPageId, end.getTime() - start.getTime());
		
		return true;
	}
	
	
	// set a batch of watch-flags in a transaction
	private boolean setWatchFlagInBatchInTransaction(String userKey, List<Long> pageIds, boolean set) {
		return transactionTemplate.execute(() -> setWatchFlagInBatch(userKey, pageIds, set));
	}
	
	
	// set a batch of watch flags
	private boolean setWatchFlagInBatch(String userKey, List<Long> pageIds, boolean set) {
		for(Long pageId : pageIds) {
			if(set == true) {
				notificationMan.addContentNotification(userAcc.getExistingUserByKey(new UserKey(userKey)), pageMan.getById(pageId));
			}
			else {
				notificationMan.removeContentNotification(userAcc.getExistingUserByKey(new UserKey(userKey)), pageMan.getById(pageId));
			}
		}
		
		return true;
	}
	
	
	// check whether a new question or a question with an added label is covered by watches
	public void handleAddAndMoveAsync(Long pageId, Long oldParentId) {
		String userKey = AuthenticatedUserThreadLocal.get() == null ? null : AuthenticatedUserThreadLocal.get().getKey().getStringValue();
		Long tick = new Date().getTime();
		CompletableFuture.supplyAsync(() -> sleepBeforeCheckingAllWatches(pageId, oldParentId, 3000, userKey, tick), pool);
	}
	
	
	// sleep and then check
	private boolean sleepBeforeCheckingAllWatches(Long pageId, Long oldParentId, int delay, String userKey, Long tick) {
		
		// sleep
		try {
			Long elapsed = new Date().getTime() - tick;
			if(elapsed < delay) {
				Thread.sleep(delay - elapsed);
			}
	
			// do our job in a transaction template
			transactionTemplate.execute(() -> handleAddAndMove(pageId, oldParentId, userKey));
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		
		return true;
	}
	
	
	// check whether the page newBranch is affected by any of the watches
	// if case set the watch for the page and all its descendants
	// and send an email to the watcher
	private boolean handleAddAndMove(Long pageId, Long oldParentId, String userKey) {
		
		Page newBranch = pageMan.getPage(pageId);
		Page oldParent = oldParentId > 0L ? pageMan.getPage(oldParentId) : null;
		
		if(newBranch == null) { return false; }
		
		// debug stuff
		Date startTime = new Date();
		log.debug("handleAddAndMove called for page id {} and old parent {}", pageId, oldParentId);
		
		// generate a list of ancestors of the new branch
		ArrayList<Page> nbAncestors = new ArrayList<Page>();
		nbAncestors.add(newBranch);
		nbAncestors.addAll(newBranch.getAncestors());
		ArrayList<Long> nbAncestorIDs = new ArrayList<Long>();
		for(Page nbAncestor : nbAncestors) {
			nbAncestorIDs.add(nbAncestor.getId());
		}
		
		log.debug("Found {} ancestors above page id {}", nbAncestors.size(), pageId);
		
		// generate a list of ancestors of the old branch
		ArrayList<Page> opAncestors = new ArrayList<Page>();
		if(oldParent != null) {
			opAncestors.add(oldParent);
			opAncestors.addAll(oldParent.getAncestors());
		}
		ArrayList<Long> opAncestorIDs = new ArrayList<Long>();
		for(Page opAncestor : opAncestors) {
			opAncestorIDs.add(opAncestor.getId());
		}
				
		// get an array of watches
		List<TreeWatch> watches = treeWatchService.getAllTreeWatches();
		
		// for each watch in the list
		for(TreeWatch watch : watches) {
			
			// check whether the old parent page (applies for a move only) was already part of the watch under evaluation
			boolean oldParentPartOfWatch = opAncestorIDs.contains(watch.getPageId());
			
			// special case related to bug #5: we are moving the watch itself
			if(watch.getID() == newBranch.getId()) {
				oldParentPartOfWatch = true;
			}
			
			// is the new branch part of the descendants of the watch?
			boolean newBranchPartOfWatch = nbAncestorIDs.contains(watch.getPageId());
						
			// extract the user key from the watch and get the user object
			ConfluenceUser watchUser = userAcc.getExistingUserByKey(new UserKey(watch.getUserKey()));
			
			// has the new parent an active watch?
			boolean newParentActiveWatch = notificationMan.isWatchingContent(watchUser, newBranch.getParent());
			
			/// we have to to something in case the new branch is part of the watch and the last parent was not part of the watch
			if(newBranchPartOfWatch && !oldParentPartOfWatch) {
												
				// in case it is a valid user
				if((watchUser != null) && (pageMan.getPage(watch.getPageId()) != null)) {
					// set the watch for that branch depending on the settings
					if(!getConfiguration().getConditionalWatch() || newParentActiveWatch) {
						
						setWatchFlag(watchUser.getKey().getStringValue(), newBranch.getId(), true);
					}
					
					// add some info to our log
					log.info("The watches for a branch where automatically added for {} starting page {}.", watchUser.getFullName(), newBranch.getTitle());
					
					// inform watchUser if indicated
					// we only send a notification by mail in case the user of the watch is different from the current user
					// and the watch user has view permission on the page where the added branch starts
					if(!watch.getUserKey().equals(userKey) && permissionMan.hasPermission(watchUser, Permission.VIEW, newBranch)) {
						sendAddMoveInNotification(watchUser, pageMan.getPage(watch.getPageId()), newBranch);
					}
				}
			}
			
			// when a branch is moved out of the watch
			// issue #8: only add a new watch in case the user can view that part
			if(oldParentPartOfWatch && !newBranchPartOfWatch && permissionMan.hasPermission(watchUser, Permission.VIEW, newBranch)) {
				if(watchUser != null) {
					// check if there is another tree watch of the user which covers to the moved branch
					if(isUserTreeWatchingPage(newBranch.getId(), watch.getUserKey()) == 0L) {
						// if not add a new tree watch on the moved part and send a notification
						// remark: we keep the watch flags unchanged here
						addWatch(watchUser, newBranch);
						sendMoveOutNotification(watchUser, pageMan.getPage(watch.getPageId()), newBranch);
					}
				}
			}
		}
		
		// debug stuff
		Date endTime = new Date();
		log.debug("handleAddAndMove took {} ms", endTime.getTime() - startTime.getTime());
		
		return true;
	}
	
	
	// check all watches whether user and page are still valid
	// if not remove the watch
	public void checkAllWatches() {
		
		// get all active tree watches
		List<TreeWatch> watches = treeWatchService.getAllTreeWatches();
		
		// for each watch in the list
		for(TreeWatch watch : watches) {
			
			try {
				log.debug("Checking tree watch {} of user {} on page {}", watch.getID(), watch.getUserKey(), watch.getPageId());
				
				// extract the page ID from the watch and check whether the page is still existing
				boolean pageValid = true;
				if(pageMan.getPage(watch.getPageId()) == null) {
					pageValid = false;
					log.debug("Invalid page found: {}", watch.getPageId());
				}
				
				// extract the user key from the watch and check whether the user is still existing
				ConfluenceUser confUser = userAcc.getExistingUserByKey(new UserKey(watch.getUserKey()));
				boolean userValid = true;
				if(confUser == null) {
					userValid = false;
					log.debug("Invalid user found: {}", watch.getUserKey());
				}
				// remark: unfortunately getExistingUserByKey does not always return null when the user is not existing any more
				// therefore I implemented this additional branch
				else {
					if(!userAcc.exists(confUser.getName())) {
						userValid = false;
						log.debug("Invalid user found: {}", watch.getUserKey());
					}
				}
				
				// issue #8: remove watches where the user has no view permission on
				if(pageValid && userValid) {
					if(!permissionMan.hasPermission(confUser, Permission.VIEW, pageMan.getPage(watch.getPageId()))) {
						pageValid = false;
					}
				}
				
				// remove watch from the allWatches string in case one piece is not valid any more
				if((pageValid == false) || (userValid == false)) {
					treeWatchService.removeWatch(watch.getPageId(), watch.getUserKey());
					log.info("The following watch was removed as it seems not to be valid any more: {}:{}", watch.getPageId(), watch.getUserKey());
				}
			}
			catch(Exception e) {
				log.error("Error handling watch {}: {}", watch.getID(), e.toString());
			}
		}
	}
	
	
	// return a link to the page with the link text = page title
	public String getTitleAsLink(Page p) {
		StringBuilder link = new StringBuilder();
		
		link.append("<a href=\"");
		link.append(settingsMan.getGlobalSettings().getBaseUrl());
		link.append("/pages/viewpage.action?pageId=");
		link.append(p.getIdAsString());
		link.append("\">");
		link.append(p.getTitle());
		link.append("</a>");
		
		return link.toString();
	}
	
	
	// send a notification when a page as been added or moved into a watch
	private void sendAddMoveInNotification(ConfluenceUser user, Page watchStart, Page branchStart) {
		
		// is there an email address set and is the user active? (bug #4)
		if((user.getEmail().length() > 0) && !userAcc.isDeactivated(user) && !getConfiguration().getAddedMailHeader().isEmpty()) {
			
			Page parent = branchStart.getParent();
						
			// generate a new message
			ConfluenceMailQueueItem mqi = new ConfluenceMailQueueItem(user.getEmail(),
				getConfiguration().getAddedMailHeader().replace("$addedTitle", branchStart.getTitle()),
				getConfiguration().getAddedMailBody().replace("$addedTitle", branchStart.getTitle()).
				replace("$addedLink", getTitleAsLink(branchStart)).
				replace("$watchTitle", watchStart.getTitle()).
				replace("$watchLink", getTitleAsLink(watchStart)).
				replace("$parentTitle", parent != null ? parent.getTitle() : "?").
				replace("$parentLink", parent != null ? getTitleAsLink(parent) : "?").
				replace("$userName", user.getFullName()).
				replace("$bodyHTML", convertStorageToMail(branchStart)),
				MIME_TYPE_HTML);
			if(mailServerMan.isDefaultSMTPMailServerDefined() && mailServerMan.getDefaultSMTPMailServer() instanceof JmxSMTPMailServer) {
				String fullname = (branchStart.getCreator() != null) ? branchStart.getLastModifier().getFullName() : "Tree View";
				mqi.setFromName(((JmxSMTPMailServer) mailServerMan.getDefaultSMTPMailServer()).getFromName().replace("${fullname}", fullname));
				log.debug(mqi.getFromName());
			}
			else {
				log.debug("No default SMTP server found!");
			}
			
			log.debug("Sending email to {} about {}.", user.getFullName(), branchStart.getTitle());
			
			// add the new message as task to the queue
			mqtm.addTask("mail", () -> mqi.send());
		}
	}
	
	
	// send a notification when a branch is moved out of a watch and is not covered by any other watch of the user
	private void sendMoveOutNotification(ConfluenceUser user, Page formerWatchStart, Page newBranch) {
		
		// is there an email address set and is the user active? (bug #4)
		if((user.getEmail().length() > 0) && !userAcc.isDeactivated(user) && !getConfiguration().getMovedOutMailHeader().isEmpty()) {
			
			Page parent = newBranch.getParent();
			
			// generate a new message
			ConfluenceMailQueueItem mqi = new ConfluenceMailQueueItem(user.getEmail(),
				getConfiguration().getMovedOutMailHeader().replace("$addedTitle", newBranch.getTitle()),
				getConfiguration().getMovedOutMailBody().replace("$addedTitle", newBranch.getTitle()).
				replace("$addedLink", getTitleAsLink(newBranch)).
				replace("$watchTitle", formerWatchStart.getTitle()).
				replace("$watchLink", getTitleAsLink(formerWatchStart)).
				replace("$parentTitle", parent != null ? parent.getTitle() : "?").
				replace("$parentLink", parent != null ? getTitleAsLink(parent) : "?").
				replace("$userName", user.getFullName()),
				MIME_TYPE_HTML);
			if(mailServerMan.isDefaultSMTPMailServerDefined() && mailServerMan.getDefaultSMTPMailServer() instanceof JmxSMTPMailServer) {
				String fullname = (newBranch.getCreator() != null) ? newBranch.getLastModifier().getFullName() : "Tree View";
				mqi.setFromName(((JmxSMTPMailServer) mailServerMan.getDefaultSMTPMailServer()).getFromName().replace("${fullname}", fullname));
				log.debug(mqi.getFromName());
			}
			else {
				log.debug("No default SMTP server found!");
			}
			
			log.debug("Sending email to {} about {}.", user.getFullName(), newBranch.getTitle());
			
			// add the new message as task to the queue
			mqtm.addTask("mail", () -> mqi.send());		
		}
	}
	
	
	// convert the storage format to email
	private String convertStorageToMail(Page p) {
		try {
			String cacheKey = p.getIdAsString() + Integer.toString(p.getVersion());
			String cachedBody = bodyCache.get(cacheKey);
			if(cachedBody != null) {
				return cachedBody;
			}
			PageContext pc = p.toPageContext();
			pc.setOutputType(ConversionContextOutputType.EMAIL.value());
			DefaultConversionContext cc = new DefaultConversionContext(pc);
			String renderedBody = xhtmlUtils.convertStorageToView(p.getBodyAsString(), cc);
			bodyCache.put(cacheKey, renderedBody);
			return renderedBody;
		}
		catch(Exception e) {
			return e.toString();
		}
	}
	

	// check if user is watching page p by one of its tree watches
	public Long isUserTreeWatchingPage(Long pageId, String userKey) {
		
		// get all watches of the user
		List<TreeWatch> usersTreeWatches = treeWatchService.getAllTreeWatchesOfUser(userKey);
		
		// get the ancestors of the page to check
		ArrayList<Page> ancestors = new ArrayList<Page>();
		ancestors.add(pageMan.getPage(pageId));
		ancestors.addAll(pageMan.getPage(pageId).getAncestors());
		
		// convert the list to list of page IDs
		ArrayList<Long> ancestorIDs = new ArrayList<Long>();
		for(Page ancestor : ancestors) {
			ancestorIDs.add(ancestor.getId());
		}
		
		// loop through all the watches
		for(TreeWatch usersTreeWatch : usersTreeWatches) {
			
			// if it contains the current page ID the user is watching the page
			if(ancestorIDs.contains(usersTreeWatch.getPageId())) {
				return usersTreeWatch.getPageId();
			}
		}
		
		// return that we found nothing
		return 0L;
	}
	
	
	// get the tree-view configuration data
	public TreeViewConfiguration getConfiguration() {
		
		// in case we don't have a valid configuration yet
		if(configuration == null) {
			
			// load it from Bandana
			configuration = getTreeViewConfigurationFromObject(bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.configuration.v1"));
			
			// in case it was not found in Bandana
			if(configuration == null) {
				
				// initialize with the default values from i18n
				configuration = new TreeViewConfiguration();
				configuration.setAddedMailHeader(i18n.getText("de.edrup.confluence.plugins.tree-view.default.added.mail.header"));
				configuration.setAddedMailBody(i18n.getText("de.edrup.confluence.plugins.tree-view.default.added.mail.body"));
				configuration.setMovedOutMailHeader(i18n.getText("de.edrup.confluence.plugins.tree-view.default.movedout.mail.header"));
				configuration.setMovedOutMailBody(i18n.getText("de.edrup.confluence.plugins.tree-view.default.movedout.mail.body"));
				configuration.setConditionalWatch(false);
			}
		}
		
		// return the configuration
		return configuration;
	}
	
	
	public TreeViewConfiguration getTreeViewConfigurationFromObject(Object o) {
		if(o == null) { return null; }
		TreeViewConfiguration conf = new TreeViewConfiguration();
		conf.setConditionalWatch((boolean) getFieldValue(o, "conditional_watch"));
		conf.setAddedMailHeader((String) getFieldValue(o, "added_mail_header"));
		conf.setAddedMailBody((String) getFieldValue(o, "added_mail_body"));
		conf.setMovedOutMailHeader((String) getFieldValue(o, "movedout_mail_header"));
		conf.setMovedOutMailBody((String) getFieldValue(o, "movedout_mail_body"));
		return conf;
	}
	
	
	private Object getFieldValue(Object o, String fieldName) {
		try  {
			Field f = o.getClass().getDeclaredField(fieldName);
			f.setAccessible(true);
			return f.get(o);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	
	// set the tree-view configuration data
	public void setConfiguration(TreeViewConfiguration configuration) {
		
		// set our local copy
		this.configuration = configuration;
		
		// and save it in Bandana
		bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.tree-view.configuration.v1", configuration);
	}
	
	
	   // check whether the plugin is licensed
    public boolean isLicensed() {
    	return pluginLicenseMan.getLicense().isDefined() ? pluginLicenseMan.getLicense().get().isValid() : false;
    }
}