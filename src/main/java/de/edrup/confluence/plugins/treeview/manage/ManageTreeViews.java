package de.edrup.confluence.plugins.treeview.manage;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.actions.AbstractUserProfileAction;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.treeview.ao.TreeWatch;
import de.edrup.confluence.plugins.treeview.ao.TreeWatchService;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class ManageTreeViews extends AbstractUserProfileAction {

	private static final long serialVersionUID = 42L;
	private final TreeWatchService treeWatchService;
	private final PageManager pageMan;
	private final PermissionManager permissionMan;

	@Inject
	public ManageTreeViews(TreeWatchService treeWatchService, @ComponentImport PageManager pageMan, @ComponentImport PermissionManager permissionMan) {
		this.treeWatchService = treeWatchService;
		this.pageMan = pageMan;
		this.permissionMan = permissionMan;
	}
	
	
	@Override
	public String execute() throws Exception
	{
		return super.execute();
	}
	
	
	public ArrayList<Page> getTreeViews() {
		
		// get the tree watches of the current user
		List<TreeWatch> watches = treeWatchService.getAllTreeWatchesOfUser(AuthenticatedUserThreadLocal.get().getKey().getStringValue());
		
		// build a list of pages
		ArrayList<Page> pagesWithTreeWatch = new ArrayList<Page>();
		for(TreeWatch watch : watches) {
			Page p = pageMan.getPage(watch.getPageId());
			if(p != null && p.isCurrent() && permissionMan.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, p)) {
				pagesWithTreeWatch.add(p);
			}
		}
		
		return pagesWithTreeWatch;
	}   
}
