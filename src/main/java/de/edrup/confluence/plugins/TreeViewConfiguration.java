package de.edrup.confluence.plugins;

import java.io.Serializable;

public class TreeViewConfiguration implements Serializable {

	// this is the first version of our configuration class
	private static final long serialVersionUID = 1L;
	
	// the relevant configuration parameters
	private String added_mail_header;
	private String added_mail_body;
	private String movedout_mail_header;
	private String movedout_mail_body;
	private boolean conditional_watch;
	
	// set and get methods for external access
	
	public void setAddedMailHeader(String header) {
		this.added_mail_header = header;
	}
	
	public String getAddedMailHeader() {
		return added_mail_header;
	}

	public void setAddedMailBody(String body) {
		this.added_mail_body = body;
	}
	
	public String getAddedMailBody() {
		return added_mail_body;
	}

	public void setMovedOutMailHeader(String header) {
		this.movedout_mail_header = header;
	}
	
	public String getMovedOutMailHeader() {
		return movedout_mail_header;
	}

	public void setMovedOutMailBody(String body) {
		this.movedout_mail_body = body;
	}
	
	public String getMovedOutMailBody() {
		return movedout_mail_body;
	}
	
	public void setConditionalWatch(boolean conditional_watch) {
		this.conditional_watch = conditional_watch;
	}
	
	public boolean getConditionalWatch() {
		return conditional_watch;
	}
}
