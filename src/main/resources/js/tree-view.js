AJS.toInit(function () {

	var watchTreeLink = AJS.contextPath() + "/pages/watch-tree.action?pageId=" + AJS.params.pageId;
	var unWatchTreeLink = AJS.contextPath() + "/pages/unwatch-tree.action?pageId=" + AJS.params.pageId;
	
	var watchTreeDiv = "<p></p><div><p><a href='" + watchTreeLink + "' title='" +
		AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.watch.tree.tooltip') + "'>" +
		AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.watch.tree.watchBox') + "</a></p></div>";
	var unWatchTreeDiv = "<div><p><a href='" + unWatchTreeLink + "' title='" +
		AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.unwatch.tree.tooltip') + "'>" +
		AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.unwatch.tree.watchBox') + "</a></p></div>";
	var notLicensedDiv = "<div class='aui-message aui-message-warning'>" + 
		AJS.I18n.getText('de.edrup.confluence.plugins.tree-view.license.fail') + "</div>";
	
	if(AJS.Meta.get("content-type") == "page") {
		$('#watch-content-button').click(function () {
			setTimeout(function () {
				$('#inline-dialog-confluence-watch form').append(watchTreeDiv).append(unWatchTreeDiv);
				AJS.$.ajax({
					url: AJS.contextPath() + "/rest/treeviewrest/1.0/isLicensed",
					type: "GET",
					dataType: "json",
					success: function(result) {
						if(result.licensed == false) {
							$('#inline-dialog-confluence-watch form').append(notLicensedDiv);
						}
					}
				});
			}, 70);
		});
	}
});